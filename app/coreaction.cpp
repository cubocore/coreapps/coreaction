/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QScreen>
#include <QScroller>
#include <QMenu>
#include <QPluginLoader>
#include <QDebug>
#include <QDir>

#include <cprime/cplugininterface.h>
#include <cprime/messageengine.h>

#include "coreaction.h"
#include "ui_coreaction.h"


coreaction::coreaction(QWidget *parent) : QWidget(parent, Qt::Dialog), ui(new Ui::coreaction)
  , smi(new settings)
{
    ui->setupUi(this);

    QPalette pltt(palette());
    pltt.setColor(QPalette::Base, Qt::transparent);
    setPalette(pltt);

    loadSettings();
    widget();
	addPlugins();
	trayicon();
}

coreaction::~coreaction()
{
	delete smi;
	delete ui;
}

void coreaction::widget()  //setup coreaction widget
{
    QRect ll;
    Q_FOREACH(auto screen, QApplication::screens()) {
        QRect ff = screen->availableGeometry();
        if (ll.height() < ff.height()){
            ll = ff;
        }
    }

    // set window size
    QScreen *scr = QGuiApplication::primaryScreen();
    QRect availableRect = scr->availableGeometry();


	qDebug() << ll << availableRect ;

    int width = ll.width();
    int height = ll.height();
    int leftPanelWidth = ll.x();
    int topPanelHeight = ll.y();
	int x = static_cast<int>(width * .23);

    ui->widgetsL->setFixedWidth(x - 5);

	if (uiMode == 2) {
        QScroller::grabGesture(ui->widgets, QScroller::LeftMouseButtonGesture);
	}

	timer.start(1000, this);

	this->setFixedSize(x, height - 5);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint /*| Qt::X11BypassWindowManagerHint */);   //| Qt::ToolTip
	this->move(width - x + leftPanelWidth, topPanelHeight);
}

void coreaction::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");

    // get app's settings
	selectedPlugins = smi->getValue("CoreAction", "SelectedPlugins");
}

void coreaction::trayicon()  //setup coreaction tryicon
{
	/* For some reason setting an svg icon directly does not work with system tray icon */
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon::fromTheme("cc.cubocore.CoreAction"));

	trayIconMenu = new QMenu(this);
    trayIconMenu->addAction( QIcon(), "&Toggle Visible", this, SLOT( show() ) );
    trayIconMenu->addAction( QIcon::fromTheme( "application-quit" ), "&Quit", QCoreApplication::instance(), SLOT( quit() ) );

    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setToolTip("CoreAction");
	trayIcon->show();

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(ShowWindow(QSystemTrayIcon::ActivationReason)));
}

void coreaction::addPlugins()
{
	/* Time */
	ui->timeW->setVisible(1);

	/* All Plugins */
    QStringList pluginPaths = selectedPlugins;

    if (not pluginPaths.count()) {
        CPrime::MessageEngine::showMessage(
            "cc.cubocore.CoreAction",
			"CoreAction",
            "Plugins not found.",
            "Please install plugins and ensure they are enabled in CoreGarage."
		);
	}

    for (int i = 0; i < selectedPlugins.count(); i++) {
        QString pPath = selectedPlugins[i];

        QPluginLoader loader(QDir::home().filePath(pPath));
        QObject *pObject = loader.instance();

        if (pObject) {
            WidgetsInterface *plugin = qobject_cast<WidgetsInterface *>(pObject);

            if (!plugin) {
				qWarning() << "Plugin Error:" << loader.errorString() << pPath;
                continue;
            }

            QWidget *w = plugin->widget(this);
            w->setFixedWidth(width() - 5);
            ui->verticalLayout_6->insertWidget(i, w);
        } else {
			qWarning() << "Plugin Error:" << loader.errorString() << pPath;
        }
    }
}

void coreaction::updateTime()
{
	QDateTime dt = QDateTime::currentDateTime();
    ui->day->setText(dt.toString("hh:mm AP\ndddd, MMM dd"));
}

void coreaction::ShowWindow(QSystemTrayIcon::ActivationReason Reason)
{
    if (Reason == QSystemTrayIcon::DoubleClick || Reason == QSystemTrayIcon::Trigger) {
        if (!this->isVisible()) {
			this->show();
//			QTimer::singleShot(8000, this, SLOT(hide()));
		}

		else {
			this->hide();
		}
	}
}

void coreaction::changeEvent( QEvent *event ) {

	if ( ( event->type() == QEvent::ActivationChange ) and ( !isActiveWindow() ) ) {
//		close();
		hide();
		event->accept();
	}

	else {
		QWidget::changeEvent( event );
		event->accept();
	}
};

void coreaction::timerEvent(QTimerEvent *event)
{
	if (timer.timerId() == event->timerId()) {
		updateTime();
	}
}
