/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QSettings>
#include <QDir>

#include <cprime/variables.h>

#include "wnotes.h"
#include "ui_wnotes.h"

#include <QDebug>
#include <QTimer>
wNotes::wNotes(QWidget *parent) : QWidget(parent), ui(new Ui::wNotes)
{
    ui->setupUi(this);

    this->setFixedHeight(this->width()*.7);

    ui->plainTextEdit->setPlaceholderText(tr("Start taking notes by typing here"));

    QDir d(CPrime::Variables::CC_Library_ConfigDir());
    d.mkdir("plugins");
    d.cd("plugins");
    d.mkdir("notes");
    d.cd("notes");
    m_configFilePath = d.path() + "/notes.conf";

	QTimer *timer = new QTimer(this);
	timer->setInterval(5000); // 5 seconds

	connect(timer, &QTimer::timeout, [this, timer](){
		if (ui->plainTextEdit->document()->isModified()) {
			saveNotes();
			ui->plainTextEdit->document()->setModified(false);
		}
		timer->stop();
	});

	connect(ui->plainTextEdit, &QPlainTextEdit::textChanged, timer, QOverload<>::of(&QTimer::start));

	collectNotes();
}

wNotes::~wNotes()
{
	if (!saveNotes()) {
		qWarning() << "Can not save notes...";
	}

    delete ui;
}

void wNotes::collectNotes()
{
    QSettings notes(m_configFilePath, QSettings::IniFormat);
    notes.beginGroup("Notes");
    ui->plainTextEdit->setPlainText(notes.value("1").toString());
    notes.endGroup();
    notes.sync();
}

bool wNotes::saveNotes()
{
    QSettings notes(m_configFilePath, QSettings::IniFormat);

    if (notes.status() == QSettings::NoError) {
        notes.beginGroup("Notes");
        notes.setValue("1", ui->plainTextEdit->toPlainText());
        notes.endGroup();
        notes.sync();
        return true;
	} else {
		return false;
	}
}

/* Name of the plugin */
QString notesPlugin::name()
{
    return "Notes";
}

/* The plugin version */
QString notesPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *notesPlugin::widget(QWidget *parent)
{
    return new wNotes(parent);
}
