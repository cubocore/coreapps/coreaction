/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QListWidgetItem>
#include <QSettings>
#include <QDebug>
#include <QDir>

#include <cprime/variables.h>

#include "calendermanage.h"
#include "wcalendar.h"
#include "ui_wcalendar.h"


wCalendar::wCalendar(QWidget *parent) : QWidget(parent), ui(new Ui::wCalendar)
{
    QString t1 = "international.txt";
    QString t2 = "country.txt";
	QDir configPath(CPrime::Variables::CC_System_ConfigDir());
	m_internationalHEventFile = configPath.filePath("coreapps/" + t1);
	m_countryHEventFile = configPath.filePath("coreapps/" + t2);

    // International holiday list collected from https://www.calendarlabs.com/holidays/international/2019
    if (!QFile(m_internationalHEventFile).exists()) {
		QString temp = QDir(RESOURCE_PATH_TEXT).filePath(t1);
		if (!QFile(temp).copy(m_internationalHEventFile)) {
			qWarning() << "Can not copy event file '" << temp << "' to '" << m_internationalHEventFile << "'";
		}
    }

    if (!QFile(m_countryHEventFile).exists()) {
		QString temp = QDir(RESOURCE_PATH_TEXT).filePath(t2);
		if (!QFile(temp).copy(m_countryHEventFile)) {
			qWarning() << "Can not copy holiday event file '" << temp << "' to '" << m_countryHEventFile << "'";
		}
    }

    ui->setupUi(this);
    ui->pages->setCurrentIndex(0);
    ui->back->setVisible(0);

    this->setFixedHeight(this->width()*.7);

    // Setup country based holiday events.
    setupCalendarEvents(m_countryHEventFile);
    // Set international holiday events
    setupCalendarEvents(m_internationalHEventFile);    
    setupReminders();

    on_calendar_currentPageChanged(QDate::currentDate().year(), QDate::currentDate().month());
}

wCalendar::~wCalendar()
{
    delete ui;
}

void wCalendar::setupCalendarEvents(const QString &eventFile)
{
    QFile file(eventFile);

	if (!file.open(QIODevice::ReadOnly)) {
		qWarning() << "Can not open event file '" << eventFile << "'";
		return;
    }

    QTextStream out(&file);

	QStringList wordList;

    QDate date;
    QString name;

    while (!out.atEnd()) {
        QString line = out.readLine();
        wordList = line.split(',');

        date = QDate::fromString(wordList.first(), "dd/MM/yyyy");
        name = wordList.last();

		m_events[date].push_back({ Holiday, name });
    }

    file.close();
}

void wCalendar::setupReminders()
{
	QSettings settings("coreapps", "reminder");
	int count = settings.value("ReminderCount").toInt();
	settings.beginGroup("All");
	QStringList list = settings.childGroups();

    std::sort(list.begin(), list.end());

    for (int i = 0; i < count; i++) {
		settings.beginGroup(list[i]);
		QDate d = settings.value("ReminderDateTime").toDateTime().date();
		m_events[d].push_back({ Reminder, settings.value("ReminderDescription").toString() });

//        QDate curr = QDate::currentDate();

//        if (d == curr) {
//            QListWidgetItem *item = new QListWidgetItem(d.toString("dd.MM.yyyy") + "\t" + settings->value("ReminderDescription").toString());
//            ui->reminder->addItem(item);
//        }
		settings.endGroup();
    }

	settings.endGroup();
}

void wCalendar::on_back_clicked()
{
    if (ui->pages->currentIndex() == 1) { //move to calendar from event details
        ui->pages->setCurrentIndex(0);
        ui->title->setText("CALENDAR");
        ui->back->setVisible(0);
    } else if (ui->pages->currentIndex() == 0) { //move to calendar from details
        ui->pages->setCurrentIndex(1);
        ui->back->setVisible(1);
    }
}

void wCalendar::on_calendar_clicked(const QDate &date)
{
    QDate d = QDate(2019, date.month(), date.day());

	if (!m_events[d].count()) {
        return;
    }

    ui->pages->setCurrentIndex(1);
    ui->back->setVisible(1);
    ui->back->setText("Back");
    ui->title->setText(date.toString());
    ui->eventDetails->clear();

	QVector<CalendarEvent> list = m_events[d];
    QString reminder;
    ui->eventDetails->setText("Holidays:");

	Q_FOREACH (const CalendarEvent &e, list) {
        if (e.type == Holiday) {
            ui->eventDetails->append(e.description);
        } else {
            reminder = reminder + e.description + "\n";
        }
    }

    ui->eventDetails->append("\nReminders:");
	ui->eventDetails->append(reminder);
}

void wCalendar::on_calendar_currentPageChanged(int year, int month)
{
    ui->calendar->clearEvents();

	Q_FOREACH (const QDate &d, m_events.keys()) {
		if (d.month() == month) {
			Q_FOREACH (const CalendarEvent &e, m_events[d]) {
				ui->calendar->addEvent(QDate(year, d.month(), d.day()), e.type);
            }
        }
    }
}

/* Name of the plugin */
QString calendarPlugin::name()
{
    return "Calendar";
}

/* The plugin version */
QString calendarPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *calendarPlugin::widget(QWidget *parent)
{
    return new wCalendar(parent);
}
