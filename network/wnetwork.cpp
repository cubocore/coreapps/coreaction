/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QTimer>

#include <cprime/filefunc.h>
#include <csys/infomanager.h>

#include "wnetwork.h"
#include "ui_wnetwork.h"


wNetwork::wNetwork(QWidget *parent) : QWidget(parent), ui(new Ui::wNetwork),
    timer(new QBasicTimer()),
	im(new InfoManager)
{
    ui->setupUi(this);

	im->setDataCount(240);
	im->setUpdateInterval(500);
	im->update();

	/** InfoManager update timer */
	QTimer *infoTimer = new QTimer(this);
	infoTimer->setTimerType( Qt::PreciseTimer );    // Precise Timer: update every 500 ms sharp
	infoTimer->setInterval( 500 );                 // We needs points every 500 ms.
	infoTimer->setSingleShot( false );              // This timer keeps repeating.

	infoTimer->callOnTimeout(
		[=]() {
			im->update();
		}
	);
	infoTimer->start();

    checkNetwork();
    usageFile = new QSettings("coreapps", "networkusage");
    currDT = QDateTime::currentDateTime();

    secCount = 0;

    // 1 second timer
    timer->start(1000, this);
    checkNetwork();

    ui->dspeed->setAttribute( Qt::WA_TransparentForMouseEvents );
    ui->dspeed->setFocusPolicy( Qt::NoFocus );

    ui->uspeed->setAttribute( Qt::WA_TransparentForMouseEvents );
    ui->uspeed->setFocusPolicy( Qt::NoFocus );
}

wNetwork::~wNetwork()
{
	delete im;
	delete timer;
	delete usageFile;
    delete ui;
}

void wNetwork::closeEvent(QCloseEvent *event)
{
	timer->stop();
    saveUsageInfo(true);
    event->accept();
}

void wNetwork::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        if (secCount == 60) { // Save usage info after 60 seconds
            saveUsageInfo(false);
            secCount = 0;
        }
        secCount++;
        checkNetwork();
    }

    event->accept();
}

void wNetwork::checkNetwork()
{
	QList<QList<double>> IOBytes = im->getNetworkSpeed(1);

	ui->dspeed->setText("↓" + CPrime::FileUtils::formatSize(IOBytes[0][0]));
	ui->uspeed->setText("↑" + CPrime::FileUtils::formatSize(IOBytes[1][0]));

}

void wNetwork::saveUsageInfo(bool close)
{
    QDateTime datetime = QDateTime::currentDateTime();

    QString monthName = datetime.toString("MMMM");
	QList<QList<double>> IOBytes = im->getNetworkSpeed(1);

    QStringList values;
	values << QString("%1").arg( IOBytes[ 0 ][0] );
	values << QString("%1").arg( IOBytes[ 1 ][1] );

    usageFile->setValue(monthName + "/" + currDT.toString("dd.MM.yyyy"), values);
    usageFile->setValue("LastChecked", datetime);
    usageFile->setValue("Close", close);
    usageFile->sync();
}

/* Name of the plugin */
QString networkPlugin::name()
{
    return "Network";
}

/* The plugin version */
QString networkPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *networkPlugin::widget(QWidget *parent)
{
    return new wNetwork(parent);
}
