/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

/*
	*
	* helloplugin.cpp - A hello world plugin for core action.
	*
*/

#include "helloplugin.hpp"
#include <QtWidgets>

/* Name of the plugin */
QString HelloPlugin::name() {

	return "Hello Plugin";
};

/* The plugin version */
QString HelloPlugin::version() {

    return "2.7.0";
};

/* The widget */
QWidget* HelloPlugin::widget( QWidget *parent ) {

	QMessageBox *box = new QMessageBox(
		QMessageBox::Information,
		"Welcome!",
        "Welcome to CuboCore suite of Applications. Hope you enjoy our Apps.",
		QMessageBox::Ok,
		parent
	);

	return box;
};
