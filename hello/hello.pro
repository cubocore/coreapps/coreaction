TEMPLATE = lib
TARGET = hello

VERSION = "4.4.0"

QT += widgets

CONFIG += plugin
CONFIG += silent

# Input
HEADERS += helloplugin.hpp
SOURCES += helloplugin.cpp
