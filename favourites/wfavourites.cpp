/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QtWidgets>

#include <cprime/systemxdg.h>
#include <cprime/applicationdialog.h>
#include <cprime/variables.h>
#include <cprime/appopenfunc.h>

#include "wfavourites.h"


/* CoreXdgMime instance */
SystemXdgMime *mimeHandler = CPrime::SystemXdgMime::instance();

wFavourites::wFavourites(QWidget *parent) : QWidget(parent)
{
    settingsManage *sm = settingsManage::initialize();
    appsList = smi->getValue("CoreApps", "Favourite");

    QLabel *lbl = new QLabel("FAVOURITES");
    lbl->setFont(QFont(font().family(), 11));
    lbl->setAlignment(Qt::AlignLeft);

    QVBoxLayout *lytM = new QVBoxLayout();
    lytM->setAlignment(Qt::AlignLeft);

    QHBoxLayout *lyt = new QHBoxLayout();
    lytM->setAlignment(Qt::AlignCenter);
    lyt->addStretch();

    QButtonGroup *appBtnGrp = new QButtonGroup();
    connect(appBtnGrp, SIGNAL(buttonClicked(int)), this, SLOT(startApp(int)));

    if (!appsList.count()) {
        lyt->addWidget(new QLabel("Favourite Apps not set."));
    } else {
        for (int i = 0; i < appsList.count(); i++) {
            QString app = appsList.value(i);
            CPrime::DesktopFile file = mimeHandler->desktopForName( app );

			QToolButton *appBtn = new QToolButton();
            appBtn->setIcon(CPrime::ThemeFunc::getAppIcon(file.desktopName()));
            appBtn->setIconSize( QSize(smi->getValue("CoreApps", "ToolBarIconSize")) );
            appBtnGrp->addButton(appBtn, i);

            lyt->addWidget(appBtn);
		}
	}

    lytM->addWidget(lbl);
    lytM->addItem(lyt);

    setLayout(lytM);
}

void wFavourites::startApp(int idx)
{
    QString app = appsList.value(idx);
    CPrime::DesktopFile file = mimeHandler->desktopForName( app );
    file.startApplication();
}

/* Name of the plugin */
QString favouritesPlugin::name()
{
    return "Favourite Apps";
}

/* The plugin version */
QString favouritesPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *favouritesPlugin::widget(QWidget *parent)
{
    return new wFavourites(parent);
}
